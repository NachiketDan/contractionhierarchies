#include <bits/stdc++.h>
#include <queue>
#include <vector>
#include <iostream>
#include <tuple>
#include "ContractionHierarchies.h"

using namespace std;

std::map <int,int> edgediff;



void calcEdgeDifference(std::vector< pair <int,int > >  graph[],int nodes)
{
/*
    EdgeDifference is primary heuristics to calculate the importance of node.
    Dijktra's algorithm is used to calculate the minimum distance between two nodes and check whether the node
    in consideration is present in shortest path.
    EdgeDifference = Number of Shortcuts introduced  - Number of Edges Incident on the node

*/
    int numberShortcuts;
    int edgeDifference;
    std::vector<std::tuple<int,int,int,int> > overlay_shortcuts;
   // C:\Users\HP\Documents\ContractionHierarchies\shortestPathStub.cpp|13|error: request for member 'size' in 'graph', which is of pointer type 'std::vector<std::pair<int, int> >*' (maybe you meant to use '->' ?)|

    for (int v = 1; v <= nodes; v++)
    {
        std::vector<int>data;
        numberShortcuts = 0;
        edgeDifference = 0;
        for(pair<int,int> e : graph[v])
        {
            data.push_back(std::get<0>(e));
        }
        for(std::vector<int>::iterator k = data.begin() ; k != data.end(); ++k)
        {
            int output = 0;
            int source ;
            int target ;
            int inter ;
            for(std::vector<int>::iterator l = k+1; l != data.end(); ++l)
            {

                source = *k;
                target = *l;
                inter = v;
                output = dijkstra(source,target,inter,graph);
                if (output > 0)
                {
                    numberShortcuts++;
                    /*
                        The below code keeps track of Source Vertex, Target Vertex , Distance , Intermediate Node.
                        This will be useful when adding shortcuts.
                    */
                    overlay_shortcuts.push_back(std::make_tuple(source,target,output,inter));
                }
            }
        }



        cout << "Number of Shortcuts are" << numberShortcuts <<"\n";
        edgeDifference = numberShortcuts - data.size();
        cout << "EdgeDifference Is" <<edgeDifference << "\n";
        edgediff.insert(std::pair<int,int> (v,edgeDifference));
    }
    /*
        Printing the data for -
        1. Edge Difference
        2. Shortcuts for a particular intermediate node.
    */

        for (std::map<int,int>::iterator it=edgediff.begin(); it!=edgediff.end(); ++it)
        {
            std::cout << it->first << " => " << it->second << '\n';

        }


        for (unsigned int i =0; i < overlay_shortcuts.size();i++)
        {
            cout << "Source " << std::get<0>(overlay_shortcuts[i]) << "\t" ;
            cout << "Target " << std::get<1>(overlay_shortcuts[i]) << "\t";
            cout << "Distance " << std::get<2>(overlay_shortcuts[i]) << "\t";
            cout << "IntermediateNode " << std::get<3>(overlay_shortcuts[i]);

            cout << "\n";
        }
}


int main()
{
    int nodes;
    printf("Enter the Number of Nodes");
    cin >> nodes;
    vector<pair<int,int>> graph[nodes+1];

    /*
    printf("Enter the Edges");
    for(int i = 0; i < edges; i++)
    {
        int a,b,w;
        cin >>a >> b >>w;
        graph[a].push_back(make_pair(b,w));
        //graph[b].push_back(make_pair(a,w));
    }
    */
    // Test Graph.
    graph[1].push_back({2, 5});
    graph[2].push_back({1, 5});

    graph[1].push_back({3, 2});
    graph[3].push_back({1, 2});

    graph[3].push_back({4, 1});
    graph[4].push_back({3, 1});

    graph[1].push_back({4, 6});
    graph[4].push_back({1, 6});

    graph[3].push_back({5, 5});
    graph[5].push_back({3, 5});



    /*
    printf("Enter the Starting vertex");
    cin>>start;
    printf("Enter the target vertex");
    cin>>target;
    printf("Enter the intermediate node");
    cin>>intermediate;
    */
    //cout<<shortestPath(N,start,graph)<<endl;
    /*
    int output = 0;
    output = dijkstra(2,3,1,graph);
    cout <<"output"<<output<<"\n";
    output = dijkstra(2,4,1,graph);
    cout <<"output"<<output<<"\n";

    */
    calcEdgeDifference(graph,nodes);

    /*
    for(int i = 1; i <= nodes ; i++)
        cout << dist[i] << " ";
    */
}





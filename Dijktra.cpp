#include <cstdio>
#include <queue>
#include <vector>
#include <iostream>
#include <string.h>
#include <set>
#include "ContractionHierarchies.h"

using namespace std;



/*void initialize() {

    for(int i = 0; i < MAX; i++)
        dist[i] = INF;
}*/


int dijkstra(int source,int target,int intermediate,vector<pair < int , int >> G[]){
    /*
        CODE TO BE USED TO CALCULATE NUMBER OF SHORTCUTS WHILE CALCULATING THE EDGE DIFFERENCE

        Function implements Dikstra's shortest path algorithm using priority queue
        It finds the shortest path from source vertex to all vertices.
        To check whether intermediate node is part of path to target from source.
        The priority queue is implemented using data-structure 'multiset' from STL.

    */

    const int MAX = 100;
    const int INF = 1e8;
    int dist[MAX];
    bool visited[MAX];

    //initialize();

    for(int i = 0; i < MAX; i++)
        dist[i] = INF;

    dist[source] = 0;                                // set all vertex as unvisited
    multiset < pair < int,int > > s;          // multiset do the job as a min-priority queue
    s.insert({0 , source});                          // insert the source node with distance = 0
    int found = 0;

    while(!s.empty()){

        pair <int , int> p = *s.begin();        // pop the vertex with the minimum distance
        s.erase(s.begin());

        int x = p.second;
        if( visited[x] ) continue;                  // check if the popped vertex is visited before

        visited[x] = true;
        if (x == intermediate)
        {
            found = 1;
        }
        if (x == target && found == 1)
        {
            return dist[x];
        }

        for(pair<int,int> e : G[x])
        {
            int adjnode = e.first;
            int weight = e.second;
            if (dist[x] + weight < dist[adjnode])
            {
                dist[adjnode] = dist[x] + weight;
                s.insert({dist[adjnode],adjnode});
            }
        }
    }
return 0;

}


